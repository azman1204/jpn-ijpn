<?php
// Create the image
$im = imagecreatefromjpeg('test.jpg');

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);

// The text to draw
$text = 'Testing...';
// Replace path by your own font path
$font = realpath('.') . '/arial.ttf';

// Add some shadow to the text
imagettftext($im, 20, 10, 52, 31, $grey, $font, $text);

// Add the text
imagettftext($im, 20, 10, 50, 30, $black, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
imagejpeg($im, 'test2.jpg');
// Format the image SRC:  data:{mime};base64,{data};
$image = file_get_contents('test2.jpg');
$imgData = base64_encode($image);
$src = 'data:image/jpeg;base64,' . $imgData;

// Echo out a sample image
echo '<img src="' . $src . '">';
imagedestroy($im);